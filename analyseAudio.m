% Matlab R2019b code to demonstrate FFT analysis of recorded audio sample
% licence: GPL 2.0
% author: Jacek Pawlyta
% date: 9.12.2020
% version: 2.1

% clear residuals from prevoius script
clear all; clc; clf;

% Set constants
% set how long the recording should last
recordTime = 6; % in s
samplingFrequency = 44100; % in Hz


% create an object for audio recording
recObj = audiorecorder(samplingFrequency,16,1);

% make some prompts
disp('Press any key to start, recording starts 2 s after a key is pressed and will last for 6 s')
pause()
pause(2)


% start audio recording for t
disp('recording started')
recordblocking(recObj, recordTime);
disp('Recording finished')
disp('------')
disp('Press any key to listen and view')
pause;

% lets play the recordig
play(recObj);
title("Audio sample recorded")
xlabel("Time, s")
ylabel("Amplitude, apparent unit")

% plot 
signalData = getaudiodata(recObj);
plot(signalData);
disp('Press any key to do FFT analysis')
pause;

% FFT analysis
analysis = fft(signalData);
sampleSize = recObj.TotalSamples;
sampling = recObj.SampleRate
powerSpectrum = analysis.*conj(analysis)/sampleSize;
fr = sampling*(1:sampleSize)/sampleSize;

plot(fr,powerSpectrum)
axis([0 0.5*max(fr) min(powerSpectrum) max(powerSpectrum)])
title("FFT analysis")
xlabel("Frequency, Hz")
ylabel("PSD, m²/Hz")

% Look for detected frequencies 
clear('frequencies');
noiseLevel = 0.1;
frequencies(1)=0;
n=1;

for k = 1:0.5*max(fr);
    if powerSpectrum(k)>noiseLevel
        frequencies(n) = fr(k);
        n= n+1;
    end;
end;

disp('Detected frequencies are :') 
frequencies

dominant = fr(find(powerSpectrum(1:0.5*max(fr))==max(powerSpectrum(1:0.5*max(fr)))));
disp(['Dominant frequency is: ', num2str(dominant), ' Hz'])
disp('press any key to exit')
pause
